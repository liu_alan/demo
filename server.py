from flask import Flask, render_template, url_for
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from db_setup import Base, Month

engine = create_engine('sqlite:///mydb.db')
Base.metadata.bind = engine
DBSession = sessionmaker(bind=engine)
session = DBSession()


app = Flask(__name__)

@app.route('/')
def welcome_page():
	js_path = url_for('static', filename='main.js')
	return render_template('main.html', js_path=js_path)


@app.route('/api/month/<month_id>', methods=['GET'])
def get_monthname_by_month_id(month_id=None):
	
	if month_id is None:
		return ''

	try:
		month_id = int(month_id)
	except ValueError:
		return 'Invalid Month Index: %s' % month_id

	month = session.query(Month).filter(Month.id==month_id).first()
	
	if month:
		return month.name
	else:
		return 'Invalid Month Index: %s' % month_id


if __name__ == '__main__':
	app.run(host='0.0.0.0', port=5000)