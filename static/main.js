function prepareDomActions(){
	var inputBox = document.getElementsByClassName('monthNum')[0];
	inputBox.oninput=getMonthName;
}

function getMonthName(){
	var inputBox = document.getElementsByClassName('monthNum')[0];
	var displayArea = document.getElementsByClassName('monthName')[0];
	var monthName;
	var inputValue = inputBox.value;
	if (isPositiveInteger(inputValue)) {
		setMonthNameByIndex(inputValue, displayArea);	
	}else{
		displayArea.textContent = '-----'
	}
}

function setMonthNameByIndex (index, displayArea) {
	var req = new XMLHttpRequest();				
	url = document.URL + 'api/month/' + index;
	req.open("GET", url, true);
	req.onreadystatechange = function () {
		var data;
		if (req.status == 200) {
			displayArea.textContent = req.responseText;
		}
	}
	req.send(null)
	
}


function isPositiveInteger (str) {
	var num = ~~Number(str);
	return String(num) === str && num > 0;
}

window.onload = prepareDomActions;