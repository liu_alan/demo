from sqlalchemy import Column, Integer, String
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class Month(Base):
	__tablename__ = 'month'

	id = Column(Integer, primary_key=True)
	name = Column(String)

	def __repr__(self):
		return "<Month(name='%s')>" % self.name


